package test;

import java.io.BufferedReader;
import java.io.FileReader;

import weka.classifiers.functions.OptStackNBSVM;
import weka.core.Instance;
import weka.core.Instances;
import weka.core.converters.ArffLoader.ArffReader;
import weka.filters.Filter;
import weka.filters.unsupervised.attribute.StringToWordVector;

public class Main {

	public static void main(String[] args) {

		OptStackNBSVM opt = new OptStackNBSVM();
		try {
			BufferedReader reader =
					new BufferedReader(new FileReader("C:/Users/CUKANOVIC/Desktop/SerbMR-3C.arff"));
			ArffReader arff = new ArffReader(reader);
			Instances data = arff.getData();
			data.setClassIndex(data.numAttributes() - 1);
			StringToWordVector stringToWord = new StringToWordVector();
		    stringToWord.setAttributeIndices("1");
		    stringToWord.setInputFormat(data);
		    data = Filter.useFilter(data, stringToWord);
		    
		    String[] options = {"-S", "1", "-C", "1.0", "-E", "0.01", "-B", "0.25", "-I", "1000" , "-A", "1.0", "-X", "1.0"};
		    opt.setOptions(options);
			opt.buildClassifier(data);
			
			for (Instance instance: data)
				opt.classifyInstance(instance);
			
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

}
