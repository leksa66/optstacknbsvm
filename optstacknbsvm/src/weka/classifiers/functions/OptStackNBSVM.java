package weka.classifiers.functions;


import java.util.ArrayList;
import java.util.Enumeration;
import java.util.List;
import java.util.Random;
import java.util.StringTokenizer;
import java.util.stream.IntStream;

import de.bwaldvogel.liblinear.SolverType;
import weka.classifiers.AbstractClassifier;
import weka.core.Attribute;
import weka.core.Capabilities;
import weka.core.Instance;
import weka.core.Instances;
import weka.core.SelectedTag;
import weka.core.Tag;
import weka.core.TechnicalInformation;
import weka.core.Utils;
import weka.core.TechnicalInformation.Type;
import weka.core.TechnicalInformationHandler;
import weka.filters.Filter;
import weka.filters.unsupervised.instance.RemoveWithValues;

/**
<!-- globalinfo-start -->
* An optimal stacks of NBSVM binary classifiers implementation. It relies on the NBSVM library. <br><br>
*	<p/>
<!-- globalinfo-end -->
*
*
*<!-- technical-bibtex-start -->
 BibTeX:
 <pre>
 &#64;article{Batanovic2016,
    author = {Vuk Batanovic, Bosko Nikolic, Milan Milosavljevic},
    pages = {2688-2696},
    title = {Reliable Baselines for Sentiment Analysis in Resource-Limited Languages: The Serbian Movie Review Dataset},
    year = {2016},
    PS = {https://vukbatanovic.github.io/pdf/LREC_2016.pdf}
 }
 
 &#64;inproceedings{Koppel2006,
    author = {Mosche Koppel, Jonathan Schler},
    pages = {100-109},
    title = {The importance of neutral examples for learning sentiment},
    year = {2006},
    PS = {http://citeseerx.ist.psu.edu/viewdoc/download?doi=10.1.1.84.9735&rep=rep1&type=pdf}
 }
 </pre>
 <br><br>
 <!-- technical-bibtex-end -->
 *
 *
<!-- options-start -->
 * Valid options are: <p/>
 *
 * <pre> -S &lt;int&gt;
 *  Set type of solver (default: 1)
 *     0 -- L2-regularized logistic regression (primal)
 *     1 -- L2-regularized L2-loss support vector classification (dual)
 *     2 -- L2-regularized L2-loss support vector classification (primal)
 *     3 -- L2-regularized L1-loss support vector classification (dual)
 *     5 -- L1-regularized L2-loss support vector classification
 *     6 -- L1-regularized logistic regression
 *     7 -- L2-regularized logistic regression (dual)</pre>
 *
 * <pre> -C &lt;double&gt;
 *  Set the cost parameter C
 *   (default: 1)</pre>
 *
 * <pre> -Z
 *  Turn on normalization of input data (default: off)</pre>
 *
 * <pre>
 * -I &lt;int&gt;
 *  The maximum number of iterations to perform.
 *  (default 1000)
 * </pre>
 *
 * <pre> -P
 *  Use probability estimation (default: off)
 * currently for L2-regularized logistic regression only! </pre>
 *
 * <pre> -E &lt;double&gt;
 *  Set tolerance of termination criterion (default: 0.001)</pre>
 *
 * <pre> -W &lt;double&gt;
 *  Set the parameters C of class i to weight[i]*C
 *   (default: 1)</pre>
 *
 * <pre> -B &lt;double&gt;
 *  Add Bias term with the given value if &gt;= 0; if &lt; 0, no bias term added (default: 1)</pre>
 *
 * <pre> -A &lt;double&gt;
 *  Set the value of the Laplace smoothing parameter alpha (default: 1.0)</pre>
 *    
 * <pre> -X &lt;double&gt;
 *  Set the value of the interpolation parameter beta (default: 0.25)</pre>
 *
 * <pre>
 * -L &lt;double&gt;
 *  The epsilon parameter in epsilon-insensitive loss function.
 *  (default 0.1)
 * </pre>
 * 
 * <pre> -D
 *  If set, classifier is run in debug mode and
 *  may output additional info to the console</pre>
 *
 <!-- options-end -->
*
* @author Aleksandar Cukanovic
* @version 1.0.0
*/
public class OptStackNBSVM extends AbstractClassifier implements TechnicalInformationHandler {

	private static final long serialVersionUID = -5528779441163733973L;
	
	public static final String REVISION = "1.0.0";
	
	/**  Number of folds used for the nested cross-validation */
	public static final int NUM_OF_FOLDS = 5;
	
	
	/** Optimal stack table used to classify instances */
	private int optStackTableSize;
	private int[] optimalStackTable;	
	
	/** NBSVM models used for training instances */
	private int numModels;
	private NBSVM[] models;

	/** Class attribute */
	private int numClasses;
	private Attribute classAttr;
	
	/** SVM solver types
     * <br>
     * Restated in this class in order to exclude the unsupported Crammer and Singer multiclassification and SV regression options from the
     * original LibLINEAR drop-down list
     * */
    public static final Tag[]      TAGS_SVMTYPE           = {new Tag(SolverType.L2R_LR.getId(), "L2-regularized logistic regression (primal)"),
            new Tag(SolverType.L2R_L2LOSS_SVC_DUAL.getId(), "L2-regularized L2-loss support vector classification (dual)"),
            new Tag(SolverType.L2R_L2LOSS_SVC.getId(), "L2-regularized L2-loss support vector classification (primal)"),
            new Tag(SolverType.L2R_L1LOSS_SVC_DUAL.getId(), "L2-regularized L1-loss support vector classification (dual)"),
            new Tag(SolverType.L1R_L2LOSS_SVC.getId(), "L1-regularized L2-loss support vector classification"),
            new Tag(SolverType.L1R_LR.getId(), "L1-regularized logistic regression"),
            new Tag(SolverType.L2R_LR_DUAL.getId(), "L2-regularized logistic regression (dual)")};
      
	/*
	 * Next parameters are needed in order to parameterize NBSVM models
	 */
	
    /** normalize input data */
    protected boolean              m_Normalize            = false;
    
	/** Laplace smoothing parameter */
	protected double m_alpha = 1.0;
	
	/** Interpolation parameter */
	protected double m_beta = 0.25;
	
	 /** the SVM solver type */
    protected SolverType           m_SolverType           = SolverType.L2R_L2LOSS_SVC_DUAL;

    /** stopping criteria */
    protected double               m_eps                  = 0.001;

    /** epsilon of epsilon-insensitive cost function **/
    protected double m_epsilon = 1e-1;

    /** cost Parameter C */
    protected double               m_Cost                 = 1;

    /** the maximum number of iterations */
    protected int m_MaxIts = 1000;

    /** bias term value */
    protected double               m_Bias                 = 1;
    
    /** whether to generate probability estimates instead of +1/-1 in case of
     * classification problems */
    protected boolean              m_ProbabilityEstimates = false;
    
    protected int[]                m_WeightLabel          = new int[0];

    protected double[]             m_Weight               = new double[0];
	  
    /**
     * List used to store class values of instances to be removed
     * with RemoveWithValues filter
     */
    private ArrayList<int[]> removeIndices;
    
    /**
     * Helper function to find indices subsets of size r in array of size n
     * 
     * @param arr		Array to find subsets
     * @param n			Size of array arr
     * @param r			Size of subsets
     * @param index		Current index in data subset
     * @param data		Subset
     * @param i			Current index in arr array
     */
    private void findIndicesToRemoveUtil(int arr[], int n, int r,
            int index, int data[], int i) {
		// Current subset is found
		if (index == r) {
			removeIndices.add(data.clone());
			return;
		}
		
		// When no more elements are there to put in data[]
		if (i >= n) return;
		
		// current is included, put next at next location
		data[index] = arr[i];
		findIndicesToRemoveUtil(arr, n, r, index + 1, data, i + 1);
		// current is excluded, replace it with next 
		findIndicesToRemoveUtil(arr, n, r, index, data, i + 1);
	}
	
	/**
	 * Function that finds all subsets of size r in arr[] of size n. 
	 *
	 * @param arr Array of class values in which we find subsets
	 * @param n   Size of arr array
	 * @param r   Size of subsets
	 */
	private void findIndicesToRemove(int arr[], int n, int r) {
		int data[] = new int[r];
		
		// Find all subsets using temporary array 'data[]'
		findIndicesToRemoveUtil(arr, n, r, 0, data, 0);
	}

    
	/**
	 * Builds the classifier.
	 * <br>
	 * We build n*(n-1)/2 binary classifiers. 
	 * In order to build each of them, we need to remove instances with the unnecessary class values and adjust class attribute.
	 * In the second part we build optimal stack table
	 * <br>
	 *
     * @param instances   The training data
     * @throws Exception  if nbsvm class encounters a problem or if the filter doesn't get applied properly
     */ 
	@Override
	public void buildClassifier(Instances instances) throws Exception {
		
		getCapabilities().testWithFail(instances);
		
		// remove instances with missing class
	    instances = new Instances(instances);
	    instances.deleteWithMissingClass();
	    
	    numClasses = instances.numClasses();

	    //number of needed binary classifiers
	    numModels = numClasses*(numClasses-1)/2;
	    models = new NBSVM[numModels];
		for (int i = 0; i < numModels; i++)
			models[i] = new NBSVM();
		
		//set options to nbsvm models
    	for (int i = 0; i < numModels; i++)
    		models[i].setOptions(getOptions());
    	
		//if number of classes is 2 then optimal stacks technique is not needed
	    if (numClasses == 2) {
	    	models[0].buildClassifier(instances);
	    }
	    else {
	    	classAttr = instances.classAttribute();
		    int classAttrIndex = classAttr.index();	       
		    
		    optStackTableSize = (int) Math.pow(2, numModels);
		    optimalStackTable = new int[optStackTableSize];
		    
		    /*
			 * According to paper The importance of neutral example for learning sentiment
			 * 5-fold cross validation is used to find optimal stacks
			 */
		    Random random = new Random(1);
		    
			instances.randomize(random);
			instances.stratify(NUM_OF_FOLDS);
			
			//array used to store counts of instances
			//per instance class value and entry in optimal stacks table			
			int[][] originalValues = new int[numClasses][optStackTableSize];
			
			for (int fold = 0; fold < NUM_OF_FOLDS; fold++) {
				
				Instances trainSet = instances.trainCV(NUM_OF_FOLDS, fold, random);
				
				RemoveWithValues removeFilter = new RemoveWithValues();
				//set class attribute
			    removeFilter.setAttributeIndex(new Integer(classAttrIndex+1).toString());
			    removeFilter.setModifyHeader(true);
			    
			    removeIndices = new ArrayList<>();
			    int[] ind = IntStream.range(0, numClasses).toArray();
			    findIndicesToRemove(ind, numClasses, numClasses - 2);
			    
			    //remove instances with unnecessary class values and train models
				for (int i = 0; i < numModels; i++) {	    
				    removeFilter.setNominalIndicesArr(removeIndices.get(i));  
				    removeFilter.setInputFormat(trainSet);
					Instances filtered = Filter.useFilter(trainSet, removeFilter); 
					
					models[i].buildClassifier(filtered);			
				}
			   	
				//used to obtain optimal stacks table per fold 
				Instances testSet = instances.testCV(NUM_OF_FOLDS, fold);	
				
				for (Instance instance: testSet) {
					double[][] predictions = new double[numModels][];
					
					for (int i = 0; i < numModels; i++) {
						predictions[i] = models[i].distributionForInstance(instance);	
					}
					
					int index = 0;
					for (int i = 0; i < numModels; i++) {
						index |= (predictions[i][0]>predictions[i][1]?0:1)<<i;
					}
					
					originalValues[(int)instance.classValue()][index]++;
				}				
			}
			
			for (int i = 0; i < optStackTableSize; i++) {
				int largest = 0;
				for (int j = 1; j < numClasses; j++) {
					if (originalValues[j][i]>originalValues[largest][i]) 
						largest = j;
						
				}
				optimalStackTable[i] = largest;
			
			}
	    } 
		
	}
	
	/**
     * Classifies a given instance.
     * <br>
     * 
     * @param instance The instance to be classified
     * @return Class
     * @throws Exception If the classifying can't be computed successfully
     */
	@Override
	public double classifyInstance(Instance instance) throws Exception {

		if (numClasses == 2) {
			double[] prediction = models[0].distributionForInstance(instance);
			return prediction[0]>prediction[1]?0:1;
		}
		else {
			double[][] predictions = new double[numModels][];
			
			for (int i = 0; i < numModels; i++) {
				predictions[i] = models[i].distributionForInstance(instance);	
			}
			
			//compare probabilities in order to obtain the order
			//use the order to read the optimal stack table
			int index = 0;
			for (int i = 0; i < numModels; i++) {
				index |= (predictions[i][0]>predictions[i][1]?0:1)<<i;
			}
			
			return optimalStackTable[index];		
		}
    }
	
	/**
     * Returns a string representation of the model including optimal stack table
     *
     * @return A string representation
     */
    @Override
    public String toString() {
    	StringBuffer sb = new StringBuffer();
    	sb.append("Weka's Optimal Stack of NBSVM binary classifiers implementation\n\n");
    	
    	if (numClasses > 2) {
    		sb.append("=== Optimal stack table ===\n");
         	
        	for (int i = 0; i < numModels; i++) {
        		int[] toRemove = removeIndices.get(i);
        		//find class values of ith model to assemble the table header
        		int[] toKeep = findIndices(IntStream.range(0, numClasses).toArray(), toRemove);
        		sb.append(classAttr.value(toKeep[0])+"/"+classAttr.value(toKeep[1])+" ");  		
        	}
        	sb.append("\tresult\n");
        	for (int i = 0; i < optStackTableSize; i++) {
        		int[] predictions = new int[numModels];
        		sb.append("\t");
        		for (int j = 0; j < numModels; j++) {
        			predictions[j] = (i>>j) & 1;
        			sb.append(predictions[j]+"\t\t");
        		}
        		
        		sb.append(classAttr.value(optimalStackTable[i])+"\n");
        	} 	
        	        		
    	}

		return sb.toString();
    }
    
    /**
     * Returns values in arr after removing those in toRemove
     * 
     * @param arr		sorted array of class values 0..numClasses-1
     * @param toRemove	sorted array of class values to remove from arr
     * @return 			remaining values
     */
    private int[] findIndices(int[] arr, int[] toRemove) {
    	int[] result = new int[2];
    	int iRem = 0, iRes = 0;
    	for (int i = 0; i < arr.length; i++) {
    		if (iRem < toRemove.length && arr[i] != toRemove[iRem]) {
    			result[iRes++] = arr[i];
    		}
    		else if (iRem == toRemove.length) {
    			while (i < arr.length) result[iRes++] = arr[i++];
    		}
    		else {
    			iRem++;
    		}
    	}
    	return result;
	}
    
	/**
     * Returns a string describing classifier
     *
     * @return A description suitable for displaying in the
     *         explorer/experimenter gui
     */
    public String globalInfo() {
		return "An optimal stack of NBSVM binary classifiers. It relies on the NBSVM library.\n\n" + getTechnicalInformation().toString();
	}
    
    /**
     * @return an instance of a TechnicalInformation object containing
     * detailed information about the original paper on NBSVM and Optimal Stacks
	 */
	@Override
	public TechnicalInformation getTechnicalInformation() {
		TechnicalInformation result;
		TechnicalInformation additional;
		
		result = new TechnicalInformation(Type.INCOLLECTION);
        result.setValue(TechnicalInformation.Field.AUTHOR, "Vuk Batanovic, Bosko Nikolic, Milan Milosavljevic");
        result.setValue(TechnicalInformation.Field.TITLE, "Reliable Baselines for Sentiment Analysis in Resource-Limited Languages:"+
        												  "The Serbian Movie Review Dataset");
        result.setValue(TechnicalInformation.Field.YEAR, "2016");
        result.setValue(TechnicalInformation.Field.PAGES, "2688-2696");
        result.setValue(TechnicalInformation.Field.LOCATION, "Belgrade, Serbia");
        result.setValue(TechnicalInformation.Field.URL, "https://vukbatanovic.github.io/pdf/LREC_2016.pdf");

        additional = result.add(Type.INPROCEEDINGS);
        additional.setValue(TechnicalInformation.Field.AUTHOR, "Mosche Koppel, Jonathan Schler");
        additional.setValue(TechnicalInformation.Field.TITLE, "The importance of neutral examples for learning sentiment.");
        additional.setValue(TechnicalInformation.Field.YEAR, "2006");
        additional.setValue(TechnicalInformation.Field.PAGES, "100-109");
        additional.setValue(TechnicalInformation.Field.LOCATION, "Ramat-Gan, Israel");
        additional.setValue(TechnicalInformation.Field.URL, "http://citeseerx.ist.psu.edu/viewdoc/download?doi=10.1.1.84.9735&rep=rep1&type=pdf");
        
        return result;
	}
    
    /**
    * Returns default capabilities of the classifier.
    *
    * @return the capabilities of this classifier
    */
    @Override
    public Capabilities getCapabilities() {   
	    return (new NBSVM()).getCapabilities();
    }
    
    /**
     * Returns an enumeration describing the available options supported in NBSVM model
     * 
     * @return An enumeration of all the available options.
     */
    @Override
    @SuppressWarnings({ "unchecked", "rawtypes" })
	public Enumeration listOptions() {
		return (new NBSVM()).listOptions();
    }
    
    /**
     * Sets the classifier options <p/>
     *
     <!-- options-start -->
     * Valid options are: <p/>
     *
     * <pre> -S &lt;int&gt;
     *  Set type of solver (default: 1)
     *   numbering is preserved from LibLINEAR
     *     0 -- L2-regularized logistic regression (primal)
     *     1 -- L2-regularized L2-loss support vector classification (dual)
     *     2 -- L2-regularized L2-loss support vector classification (primal)
     *     3 -- L2-regularized L1-loss support vector classification (dual)
     *     5 -- L1-regularized L2-loss support vector classification
     *     6 -- L1-regularized logistic regression
     *     7 -- L2-regularized logistic regression (dual)</pre>
     *
     * <pre> -C &lt;double&gt;
     *  Set the cost parameter C
     *   (default: 1)</pre>
     *
     * <pre> -Z
     *  Turn on normalization of input data (default: off)</pre>
     *
     * <pre>
     * -I &lt;int&gt;
     *  The maximum number of iterations to perform.
     *  (default 1000)
     * </pre>
     *
     * <pre> -P
     *  Use probability estimation (default: off)
     * currently for L2-regularized logistic regression only! </pre>
     *
     * <pre> -E &lt;double&gt;
     *  Set tolerance of termination criterion (default: 0.001)</pre>
     *
     * <pre> -W &lt;double&gt;
     *  Set the parameters C of class i to weight[i]*C
     *   (default: 1)</pre>
     *
     * <pre> -B &lt;double&gt;
     *  Add Bias term with the given value if &gt;= 0; if &lt; 0, no bias term added (default: 1)</pre>
     *  
     * <pre> -A &lt;double&gt;
     *  Set the value of the Laplace smoothing parameter alpha (default: 1.0)</pre>
     *    
     * <pre> -X &lt;double&gt;
     *  Set the value of the interpolation parameter beta (default: 0.25)</pre>
     *
     * <pre>
     * -L &lt;double&gt;
     *  The epsilon parameter in epsilon-insensitive loss function.
     *  (default 0.1)
     * </pre>
     * 
     * <pre> -D
     *  If set, classifier is run in debug mode and
     *  may output additional info to the console</pre>
     *
     <!-- options-end -->
     *
     * @param options The options to parse
     * @throws Exception if parsing fails
     */
    @Override
    public void setOptions(String[] options) throws Exception {
    	
    	String tmpStr = Utils.getOption('A', options);
        if (tmpStr.length() != 0)
            setAlpha(Double.parseDouble(tmpStr));
        else
            setAlpha(1.0);
        
        tmpStr = Utils.getOption('X', options);
        if (tmpStr.length() != 0)
            setBeta(Double.parseDouble(tmpStr));
        else
            setBeta(0.25);
        
        tmpStr = Utils.getOption('S', options);
        if (tmpStr.length() != 0)
            setSVMType(new SelectedTag(Integer.parseInt(tmpStr), TAGS_SVMTYPE));
        else
            setSVMType(new SelectedTag(SolverType.L2R_L2LOSS_SVC_DUAL.getId(), TAGS_SVMTYPE));

        tmpStr = Utils.getOption('C', options);
        if (tmpStr.length() != 0)
            setCost(Double.parseDouble(tmpStr));
        else
            setCost(1);

        tmpStr = Utils.getOption('E', options);
        if (tmpStr.length() != 0)
            setEps(Double.parseDouble(tmpStr));
        else
            setEps(0.001);

        setNormalize(Utils.getFlag('Z', options));

        tmpStr = Utils.getOption('B', options);
        if (tmpStr.length() != 0)
            setBias(Double.parseDouble(tmpStr));
        else
            setBias(1);

        setWeights(Utils.getOption('W', options));

        setProbabilityEstimates(Utils.getFlag('P', options));

        tmpStr = Utils.getOption('L', options);
        if (tmpStr.length() != 0) {
            setEpsilon(Double.parseDouble(tmpStr));
        } else {
            setEpsilon(0.1);
        }

        tmpStr = Utils.getOption('I', options);
        if (tmpStr.length() != 0) {
            setMaximumNumberOfIterations(Integer.parseInt(tmpStr));
        } else {
            setMaximumNumberOfIterations(1000);
        }
        
        super.setOptions(options);
    }
    
    /**
     * Returns the current options
     *
     * @return The current setup
     */
    @Override
    public String[] getOptions() {
    	 List<String> options = new ArrayList<String>();

         options.add("-S");
         options.add("" + m_SolverType.getId());

         options.add("-C");
         options.add("" + getCost());

         options.add("-E");
         options.add("" + getEps());

         options.add("-B");
         options.add("" + getBias());

         if (getNormalize()) options.add("-Z");

         if (getWeights().length() != 0) {
             options.add("-W");
             options.add("" + getWeights());
         }

         if (getProbabilityEstimates()) options.add("-P");
         
         options.add("-I");
         options.add("" + getMaximumNumberOfIterations());
         
         options.add("-A");
         options.add("" + getAlpha());
         
         options.add("-X");
         options.add("" + getBeta());
         
         options.add("-L");
         options.add("" + getEpsilon());

         return options.toArray(new String[options.size()]);
    }
    
	/**
     * Returns the revision string.
     *
     * @return The revision
     */
    @Override
    public String getRevision() {
    	return REVISION;
    }
    
    /**
	 * Sets the Laplace smoothing parameter
	 * 
	 * @param alpha Laplace smoothing parameter
	 */
	public void setAlpha (double alpha) {
		this.m_alpha = alpha;
	}
	
	/**
	 * Returns the Laplace smoothing parameter
	 * 
	 * @return current Laplace smoothing parameter
	 */
	public double getAlpha () {
		return m_alpha;
	}
	
	/**
	 * Sets the interpolation parameter beta
	 * 
	 * @param beta the interpolation parameter
	 */
	public void setBeta (double beta) {
		this.m_beta = beta;
	}
	
	/**
	 * Returns the interpolation parameter beta
	 * 
	 * @return current interpolation parameter
	 */
	public double getBeta () {
		return m_beta;
	}

	/**
	 * Sets the solver type
	 * 
	 * @param m_SolverType solver type
	 */
	public void setSolverType(SolverType m_SolverType) {
		this.m_SolverType = m_SolverType;
	}

	/**
     * Sets tolerance of termination criterion (default 0.001)
     *
     * @param value       the tolerance
     */
    public void setEps(double value) {
        m_eps = value;
    }

    /**
     * Gets tolerance of termination criterion
     *
     * @return            the current tolerance
     */
    public double getEps() {
        return m_eps;
    }

	/**
     * Get the value of epsilon parameter of the epsilon insensitive loss
     * function.
     *
     * @return Value of epsilon parameter.
     */
    public double getEpsilon() {
        return m_epsilon;
    }

    /**
     * Set the value of epsilon parameter of the epsilon insensitive loss
     * function.
     *
     * @param v Value to assign to epsilon parameter.
     */
    public void setEpsilon(double v) {
        m_epsilon = v;
    }

    /**
     * Sets the cost parameter C (default 1)
     *
     * @param value       the cost value
     */
    public void setCost(double value) {
        m_Cost = value;
    }

    /**
     * Returns the cost parameter C
     *
     * @return            the cost value
     */
    public double getCost() {
        return m_Cost;
    }

	/**
     * Get the number of iterations to perform.
     *
     * @return maximum number of iterations to perform.
     */
    public int getMaximumNumberOfIterations() {
        return m_MaxIts;
    }

    /**
     * Set the number of iterations to perform.
     *
     * @param v the number of iterations to perform.
     */
    public void setMaximumNumberOfIterations(int v) {
        m_MaxIts = v;
    }

    /**
     * Sets bias term value (default 1)
     * No bias term is added if value &lt; 0
     *
     * @param value       the bias term value
     */
    public void setBias(double value) {
        m_Bias = value;
    }

    /**
     * Returns bias term value (default 1)
     * No bias term is added if value &lt; 0
     *
     * @return             the bias term value
     */
    public double getBias() {
        return m_Bias;
    }
	
    /**
     * whether to normalize input data
     *
     * @param value       whether to normalize the data
     */
    public void setNormalize(boolean value) {
        m_Normalize = value;
    }

    /**
     * whether to normalize input data
     *
     * @return            true, if the data is normalized
     */
    public boolean getNormalize() {
        return m_Normalize;
    }

    /**
     * Returns whether probability estimates are generated instead of -1/+1 for
     * classification problems.
     *
     * @param value       whether to predict probabilities
     */
    public void setProbabilityEstimates(boolean value) {
        m_ProbabilityEstimates = value;
    }

    /**
     * Sets whether to generate probability estimates instead of -1/+1 for
     * classification problems.
     *
     * @return            true, if probability estimates should be returned
     */
    public boolean getProbabilityEstimates() {
        return m_ProbabilityEstimates;
    }

	 /**
     * Sets the parameters C of class i to weight[i]*C (default 1).
     * Blank separated list of doubles.
     *
     * @param weightsStr          the weights (doubles, separated by blanks)
     */
    public void setWeights(String weightsStr) {
        StringTokenizer tok = new StringTokenizer(weightsStr, " ");
        m_Weight = new double[tok.countTokens()];
        m_WeightLabel = new int[tok.countTokens()];

        if (m_Weight.length == 0) System.out.println("Zero Weights processed. Default weights will be used");

        for (int i = 0; i < m_Weight.length; i++) {
            m_Weight[i] = Double.parseDouble(tok.nextToken());
            m_WeightLabel[i] = i;
        }
    }

    /**
     * Gets the parameters C of class i to weight[i]*C (default 1).
     * Blank separated doubles.
     *
     * @return            the weights (doubles separated by blanks)
     */
    public String getWeights() {

        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < m_Weight.length; i++) {
            if (i > 0) sb.append(" ");
            sb.append(m_Weight[i]);
        }

        return sb.toString();
    }

	/**
     * Sets the type of SVM (default SVMTYPE_L2)
     *
     * @param value The type of the SVM
     */
    public void setSVMType(SelectedTag value) {
        if (value.getTags() == TAGS_SVMTYPE) {
            setSolverType(SolverType.getById(value.getSelectedTag().getID()));
        }
    }
    
    /**
     * Gets the type of SVM
     *
     * @return The type of the SVM
     */
    public SelectedTag getSVMType() {
        return new SelectedTag(m_SolverType.getId(), TAGS_SVMTYPE);
    }


}
